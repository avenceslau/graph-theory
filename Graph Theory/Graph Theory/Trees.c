//
//  Trees.c
//  Graph Theory
//
//  Created by André Venceslau on 02/03/2021.
//

#include "Trees.h"
#define max(a,b)     ((a) > (b) ? (a) : (b))


void Abort(void * allocated_mem, trunk * table, int n_vertex){
    if (allocated_mem == NULL) {
        treeCleanMem(table, n_vertex);
        exit(0);
    }
}

void treeCut(node * tree){
    if(tree != NULL){
        if(tree->left != NULL){ treeCut(tree->left);}
        if(tree->right != NULL){ treeCut(tree->right);}
        free(tree);
    }
}

int treeHeight(node *n) {
    if (n == NULL){
        return 0;
    }
    return n->height;
}

void treeCutTrunk(trunk *table){
    free(table);
}

node * treeGetRoot(trunk * table, int key){
    return table[key].root;
}

int treeGetLinks(trunk * table, int key){
    return table[key].n_links;
}

void treeInitVertex(trunk *table) {
    (*table).n_links = 0;
    (*table).root = NULL;
}

void treeInit(trunk * table, int n_vertex){
    for (int i = 0 ; i < n_vertex ; i++) {
        treeInitVertex(&table[i]);
    }
}

node * avlCreateLeaf(int name, double cost, _data data) {
    node * new = (node *) malloc(sizeof(node));
    if (new == NULL) {
        Abort(NULL, data.table, data.max_vertex);
        exit(0);
    }
    new->left = NULL;
    new->right = NULL;
    new->cost = cost;
    new->name = name;
    new->height = 1;
    return new;
}

int avlBalance (node * root) {
    if(root == NULL)
        return 0;
    
    return treeHeight(root->left)-treeHeight(root->right);
}

trunk * treeCreateTrunk(int n_vertex){
    trunk *table = (trunk *) malloc(sizeof(trunk)*n_vertex);
    if (table == NULL) {
        exit(0);
    }
    treeInit(table, n_vertex);
    return table;
}

node * treeLeftOf(node * t){
    return t->left;
}

node * treeRightOf(node * t){
    return t->right;
}

node * avlRotateRight(node * y){
    node * x = y->left;
    node * z = x->right;
    
    x->right = y;
    y->left = z;
    
    y->height = 1 + max(treeHeight(y->left), treeHeight(y->right));
    x->height = 1 + max(treeHeight(x->left), treeHeight(x->right));
    
    return x;
}

node * avlRotateLeft(node * x){
    node * y = x->right;
    node * z = y->left;
    
    y->left = x;
    x->right = z;
    
    x->height = 1 + max(treeHeight(x->left), treeHeight(x->right));
    y->height = 1 + max(treeHeight(y->left), treeHeight(y->right));
    
    return y;
}




node * avlInsert(node * root, int name, double cost, _data data){
  
    if (root == NULL) { return(avlCreateLeaf(name, cost, data)); }

    if (name < root->name) {
        root->left  = avlInsert(root->left, name, cost, data);
        
    } else if (name > root->name) {
        root->right  = avlInsert(root->right, name, cost, data);
        
    } else { return root; }
    
    root->height = 1 + max(treeHeight(root->left), treeHeight(root->right));
    int bal = avlBalance(root);
    
    if(bal >  1 && name < root->left->name)  { return avlRotateRight(root);}
    else if(bal < -1 && name > root->right->name) { return avlRotateLeft(root); }
    else if(bal >  1 && name > root->left->name)  {
        root->left = avlRotateLeft(root->left);
        return avlRotateRight(root);
    }
    else if(bal < -1 && name < root->right->name) {
        root->right = avlRotateRight(root->right);
        return avlRotateLeft(root);
    }
    return root;
}

//Vê-se numa árvore existe uma ligação a um node "name" name = id do node
node * treeFindNode(node *tree, int name){
    if((tree)==NULL){
        return NULL;
    }
    
    if(name == (tree)->name){
        return (tree);
    } else if(name < (tree)->name){
        return treeFindNode((tree)->left, name);
    } else if (name > (tree)->name){
        return treeFindNode((tree)->right, name);
    } else {
        return NULL;
    }
}

//Recebe um array x posições onde x = n_links de uma árvore
//guarda em cada posição do array um dos nodes
void treeSeeEveryBranch(node * tree, int array[], int *i){
    if(!tree){
        return;
    }
    array[(*i)] = tree->name;
    (*i)++;
    if (!(tree->left) && !(tree->right) ) { //Dead Ends
        
        return;
    }
    
    if (tree->left) {
        treeSeeEveryBranch(tree->left, array, i);
    }
    if (tree->right) {
        treeSeeEveryBranch(tree->right, array, i);
    }
}

void treeCleanMem(trunk * table, int n_vertex){
    for (int i = 0; i < n_vertex; i++) {
        treeCut(table[i].root);
    }
    treeCutTrunk(table);
}

