//
//  Connected_Components.h
//  Graph Theory
//
//  Created by André Venceslau on 01/03/2021.
//

#ifndef Connected_Components_h
#define Connected_Components_h

#include <stdio.h>

int findCompressed(int x, int*set);
void setUnion(int x, int y, int * set, int * sz);
void linkSet(int x, int y, int * set);
void makeSet(int x, int*set, int *sz);
void makeSetNoSize(int x, int*set);
int findSize(int x, int *sz, int * set);
int sizeOfComponentContainingV(int * component, int v, int n_vertex);
int * componentOfV(int * component, int v, int n_vertex, int * number_vertex_in_scc);

#endif /* Connected_Components_h */
