//
//  Connected_Components.c
//  Graph Theory
//
//  Created by André Venceslau on 01/03/2021.
//

#include "Connected_Components.h"

int findCompressed(int x, int*set){
    int root = x;
    while (root != set[root]) {
        root = set[root];
    }
    while (x != root) {
        int next = set[x];
        set[x] = root;
        x = next;
    }
    return root;
}

//Une dois conjuntos com verificação dos seus pesos
void setUnion(int x, int y, int * set, int * sz){
    x = findCompressed(x, set);
    y = findCompressed(y, set);
    
    if (x == y) {
        return;
    }
    if (sz[x] < sz[y]) {
        set[x] = y;
        sz[y] = sz[x] + sz[y];
    } else {
        set[y] = x;
        sz[x] = sz[x] + sz[y];
    }
}

//Une dois conjuntos sem verificação de peso, e y aponta para conjunto de x
void linkSet(int x, int y, int * set){
    x = findCompressed(x, set);
    y = findCompressed(y, set);
    if (x == y) {
        return;
    }
    set[y] = x;
}

//Para um valor x, cria um conjunto isolado para x com peso 1
void makeSet(int x, int*set, int *sz){
    if (set[x] == 0) {
        set[x] = x;
        sz[x] = 1;
    }
}

//Para um valor x, cria um conjunto isolado para x
void makeSetNoSize(int x, int*set){
    if (set[x] == 0) {
        set[x] = x;
    }
}

//Encontra o peso do conjunto do valor x
int findSize(int x, int *sz, int * set){
    return sz[findCompressed(x, set)];
}

int sizeOfComponentContainingV(int * component, int v, int n_vertex) {
    int v_component = findCompressed(v, component);
    int count = 0;
    for (int  i = 0; i < n_vertex; i++) {
        if (i != v){
            if (v_component == findCompressed(i, component)) {
                count++;
            }
        }
    }
    return count;
}

int * componentOfV(int * component, int v, int n_vertex, int * number_vertex_in_scc) {
    int size_of_v_component = sizeOfComponentContainingV(component, v, n_vertex);
    int count = 0;
    int * component_of_v = (int *) malloc(sizeof(int)*size_of_v_component);
    for (int i = 0 ; i < n_vertex ; i++) {
        if (i != v) {
            if (findCompressed(v, component) == findCompressed(i, component)) {
                component_of_v[count] = i;
                count++;
            }   
        }
    }
    (*number_vertex_in_scc) = count;
}