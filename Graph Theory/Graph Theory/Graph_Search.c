//
//  Graph_Search.c
//  Graph Theory
//
//  Created by André Venceslau on 14/11/2020.
//

#include "Graph_Search.h"
#include "Storing_Graphs.h"
#include "Stacks_Queues_PQueues.h"
#include "Connected_Components.h"
#include "Baders.h"
#include <limits.h>
#include <float.h>

#define MaxV 10000
#define maxV 10000
#define P G->adj[v][w]
#define GRAPHiso(G,v) (GRAPHdeg(G,v) == 0)

Edge nn[maxV];
static int cnt, pre[MaxV];

Edge *EDGE(int v, int w){
    Edge * e = (Edge *) malloc(sizeof(Edge));
    e->v = v;
    e->w = w;
    return e;
}

int Val(link *v, int w){
    link * aux = NULL;
    do {
        if (aux->v == w) {
            return aux->value;
        }
        aux = aux->next;
    } while (aux != NULL);
    return 0;
}



void dfsRMatrix(GraphM *G, Edge *e) {
    int t, w = e->w;
    pre[w] = cnt++;
    for (t = 0; t < G->V; t++){
        if (G->adj[w][t] != 0){
            if (pre[t] == -1){
                Edge * z = EDGE(w, t);
                dfsRMatrix(G, z);
            }
        }
    }
}

void dfsRList(GraphL *G, Edge *e){
    link *t;
    int w = e->w;
    pre[w] = cnt++;
    for (t = G->adj[w]; t != NULL ; t = t->next){
        if (pre[t->v] == -1){
            Edge * z = EDGE(w, t->v);
            dfsRList(G, z);
            free(z);
        }
    }
    
}

void bfsMatrix(GraphM *G, Edge *e) {
    int v;
    int * st = malloc(sizeof(int)*G->V);
    Queue * q = NULL;
    queuePut(q, e);
    while (!queueIsEmpty(q)){
        e = (Edge *) queueGet(q);
        if (pre[e->w] == -1) {
            pre[e->w] = cnt++;
            st[e->w] = e->v;
            for (v = 0; v < G->V; v++)
                if (G->adj[e->w][v] == 1)
                    if (pre[v] == -1){
                        Edge * ex = (Edge *) malloc(sizeof(Edge));
                        ex->v = e->w; ex->w = v;
                        queuePut(q, ex);
                    }
        }
        free(e);
    }
    queueErase(q);
}

//LACKS STACK
//FIXME
void bfsList(GraphL *G, Edge *e) {
    int v;
    int * st = (int *) malloc(sizeof(int)*G->V);
    Queue * q = NULL;
    queuePut(q,e);
    while (!queueIsEmpty(q)){
        e = (Edge *) queueGet(q);
        if (pre[e->w] == -1) {
            pre[e->w] = cnt++;
            st[e->w] = e->v;
            for (v = 0; v < G->V; v++)
            if (ListFindEdge(G->adj[e->w], v)->next != NULL)
                if (pre[v] == -1){
                    Edge * ex = (Edge *) malloc(sizeof(Edge));
                    ex->v = e->w; ex->w = v;
                    queuePut(q, ex);
                }
        }
        free(e);
    }
    queueErase(q);
}

int minKey(double *cost, bool *mstSet,GraphM *g){
    int v;
    double min = DBL_MAX;
    int min_index = 0;
    for ( v = 0; v < g->V; v++)   {
        if (mstSet[v] == false && cost[v] < min){
            min = cost[v];
            min_index = v;
        }
        
    }
    return min_index;
}


void primMST(GraphM *G) {
    // Array to store constructed MST
    int *parent = (int *) malloc(sizeof(int));
    // Key values used to pick minimum weight edge in cut
    double *key = (double *) malloc(sizeof(double));
    // To represent set of vertices included in MST
    bool * mstSet = (bool *) malloc(sizeof(bool));
    
    // Initialize all keys as INFINITE
    for (int i = 0; i < G->V; i++){
        key[i] = DBL_MAX; mstSet[i] = false;
    }
    
    // Always include first 1st vertex in MST.
    // Make key 0 so that this vertex is picked as first vertex.
    key[0] = 0;
    parent[0] = -1; // First node is always root of MST
    
    // The MST will have V vertices
    for (int count = 0; count < G->V - 1; count++) {
        // Pick the minimum key vertex from the
        // set of vertices not yet included in MST
        int u = minKey(key, mstSet, G);
        
        // Add the picked vertex to the MST Set
        mstSet[u] = true;
        
        // Update key value and parent index of
        // the adjacent vertices of the picked vertex.
        // Consider only those vertices which are not
        // yet included in MST
        for (int v = 0; v < G->V; v++){
            
            // graph[u][v] is non zero only for adjacent vertices of m
            // mstSet[v] is false for vertices not yet included in MST
            // Update the key only if graph[u][v] is smaller than key[v]
            if (G->adj[u][v] && mstSet[v] == false && G->adj[u][v] < key[v]){
                parent[v] = u;
                key[v] = G->adj[u][v];
            }
        }
    }
}


//Algoritmo de Kruskal
void kruskalAlgo(Edge *g, int n_vertex, int n_links) {
    int i, cno1, cno2;
    int links_in_mst = 0;
    int  *belongs = (int *) calloc(sizeof(int), n_vertex);
    
    qsort(g, 0, n_links-1, GREATER);
    
    for (i = 0; i < n_vertex; i++){
        makeSetNoSize(i, belongs);
    }
    
    for (i = n_links-1; i >= 0 ; i--) {
        cno1 = findCompressed(g[i].w, belongs);
        cno2 = findCompressed(g[i].v, belongs);
        
        if (cno1 != cno2) {
            links_in_mst++;
            linkSet(cno2, cno1, belongs);
            if (links_in_mst == (n_vertex-1)) {
                break;
            }
        }
    }
    free(belongs);
}



static int visited[maxV];

int pathR(GraphM *G, int v, int w) {
    int t;
    if (v == w) return 1; visited[v] = 1;
    for (t = 0; t < G->V; t++)
    if (G->adj[v][t] == 1)
        if (visited[t] == 0)
            if (pathR(G, t, w)) return 1;
    return 0;
}

int GRAPHpath(GraphM *G, int v, int w) {
    int t;
    for (t = 0; t < G->V; t++)
    visited[t] = 0;
    return pathR(G, v, w);
}

int bfsSCC(int starting_vertex, int number_of_steps, int * components, GraphM *g){
    int number_vertex_in_scc = 0;
    int * v_scc = componentOfV(components, starting_vertex, g->V, &number_vertex_in_scc);
    int * visited = (int *) calloc(g->V, sizeof(int));
    Queue * q = queuePut(q, &starting_vertex);

    while (!queueIsEmpty(q)) {
        int w = queueGet(q);
        if (!visited[w]) {
            visited[w] = 1;
            for (int i = 0 ; i < number_vertex_in_scc ; i++) {
                if ((g->adj[w][v_scc[i]] != 0) && (visited[v_scc[i]] == 0)) {
                    queuePut(q, &v_scc[i]);
                }
            }
        }
    }
}