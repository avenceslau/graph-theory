//
//  Stacks_Queues_PQueues.h
//  Graph Theory
//
//  Created by André Venceslau on 01/03/2021.
//

#ifndef Stacks_Queues_PQueues_h
#define Stacks_Queues_PQueues_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool compare(void * item1, void * item2);

typedef struct {
    void ** data;
    int n_elements;
    int size;
} Stack;

Stack * stackInit(int size);
void stackPush(Stack *stack, void * item);
void * stackTop(Stack *stack);
void stackPop(Stack *stack);
bool stackIsEmpty(Stack *stack);
void stackErase(Stack * stack);
void stackPrint(Stack * stack, void printItem(const void*));


typedef struct _q{
    struct _q * next;
    void * item;
} qNode;

typedef struct {
    qNode * top;
    qNode * bottom;
} Queue;

Queue * queuePut(Queue *q, void * item);
void * queueGet(Queue * q);
bool queueIsEmpty(Queue * q);
void queueErase(Queue * q);
void queuePrint(Queue * q, void printItem(const void *));

typedef struct {
    void ** data;
    int * priority;
    int n_elements;
    int size;
} PQueue;


PQueue * pqCreate(int size);
void pqAdd(PQueue * q, void * item);
void pqModify(PQueue * q, int index, void * newpriority);
void pqMinDel(PQueue * h);
void pqErase(PQueue * q);
void * pqGetFirst(PQueue * q);
void heapSort(PQueue * h, int compare(const void *, const void *));

void pqPrint(PQueue * q, void printItem(const void *));

#endif /* Stacks_Queues_PQueues_h */
