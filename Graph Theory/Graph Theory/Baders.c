//
//  Baders.c
//  Graph Theory
//
//  Created by André Venceslau on 01/03/2021.
//

#include "Baders.h"
#include "Storing_Graphs.h"
#include <stdlib.h>
#include "Connected_Components.h"

#define min(a,b)     ((a) < (b) ? (a) : (b))
#define max(a,b)     ((a) > (b) ? (a) : (b))
/*
This function must be adapted to depending on the data structure use
 For example on a list on might do list[v] and get v->u
 On a matrix should be easier matrix[v][u] which is close to an hash table
*/
double getCost(int u, int v){
    return 1;
}

int GREATER(const void * A, const void * B){
    Edge * x = (Edge *) A;
    Edge * y = (Edge *) B;
    if(getCost(y->v, y->w) > getCost(x->v, x->w)){
        return 0;
    } else {
        return 1;
    }
}



//Procura em profundidade (Depth-First Search DFS) na MST
void dfsMST(int v, int * in, int *out, int *counter, int *parents, Edge * mst, int n_links){
    (*counter)++;
    in[v] = *counter;
        
    for (int i = 0; i < n_links; i++) {
        Edge e = mst[i];
        if (e.v == v && !in[e.w]) {
            parents[e.w] = v;
            dfsMST(e.w, in, out, counter, parents, mst, n_links);
            (*counter)++;
        }
        if (e.w == v && !in[e.v]) {
            parents[e.v] = v;
            dfsMST(e.v, in, out, counter, parents, mst, n_links);
            (*counter)++;
        }
    }
    out[v] = *counter;
}

//Encontra para uma ligação que não pertença à MST as ligações que substitui
void pathLabel(int s, int t, Edge e, int *in, int *out, int *parents, int *set, Edge *MST , Edge *redges, int *r_links){
    int plan = 0;
    int k1 = 0, k2 = 0;
    if (in[s] < in[t] && in[t] < out[s]) { //s is ancestor of t
        return;
    }
    if (in[t] < in[s] && in[s] < out[t]) {  //t is ancestor of s
        plan = 1; //ancestor
        k1 = in[t];
        k2 = in[s];
    } else if (in[s] < in[t]) {
        plan = 2; //LEFT
        k1 = out[s];
        k2 = in[t];
    } else {
        plan = 3; //RIGHT
        k1 = out[t];
        k2 = in[s];
    }
    int v = s;
    while (k1 < k2) {
        if(findCompressed(v, set) == v){
            
            //data type edge u = menor vertex, v = maior vertex
            MST[(*r_links)].w = min(v, parents[v]); //saves mst edge
            MST[(*r_links)].v = max(v, parents[v]);
            redges[(*r_links)].w = min(e.w, e.v); //saves corresponding best replacement edge
            redges[(*r_links)].v = max(e.w, e.v);
            (*r_links)++;
            linkSet(parents[v],v, set);
        }
        v = parents[v];
        switch (plan) {
            case 1:
                k2 = in[v];
                break;
            case 2:
                k1 = out[v];
                break;
            case 3:
                k2 = in[v];
                break;
            default:
                break;
        }
    }
}

//Encontra todas as ligações substitutas de uma MST
//Recebe uma MST e as ligações que não pertencem à MST ordenadas por custo crescente
//Retorna dois arrays de ligações: vetor contendo as ligações da MST, e outro que contém a sua substituta
void FindReplacementEdges(int n_vertex, int non_mst_links, int mst_n_links, int *r_links, Edge *nonMST, Edge *MST, int v, Edge * r_edges){ // v is KEY(vertex)
    int counter = 0;
    int *in = (int *) calloc(sizeof(int), n_vertex);
    int *out = (int *) calloc(sizeof(int), n_vertex);
    int *set = (int *) calloc(sizeof(int), n_vertex); // saves size of disjoint set
    int *parents = (int *) calloc(sizeof(int), n_vertex);
    
    parents[v] = v;
    counter = 0;
    dfsMST(v, in, out, &counter, parents, MST, mst_n_links);
    
    for (int i = 0; i < n_vertex; i++) {
        makeSetNoSize(i, set);
    }
    
    for (int i = *r_links ; i < mst_n_links ; i++) {
        r_edges[i].w = -1;
        r_edges[i].v = -1;
    }
    
    
    for (int i = non_mst_links -1; i >= 0; i--) {
        Edge e = nonMST[i];
        pathLabel(e.w, e.v, e, in, out, parents, set, MST, r_edges, r_links);
        pathLabel(e.v, e.w, e, in, out, parents, set, MST, r_edges, r_links);
    }
    free(in);
    free(out);
    free(set);
    free(parents);
}
