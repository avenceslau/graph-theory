//
//  Trees.h
//  Graph Theory
//
//  Created by André Venceslau on 02/03/2021.
//

#ifndef Trees_h
#define Trees_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define DUPLICATED_NODE -2
#define NO_MEMORY -1

typedef struct _node node;
typedef struct _trunk trunk;

struct _node{
    int name; //used to access trunk
    int height; //takes values between -2 and 2, is utilized to balance tree through rotations that shift the weight of a branch to another
    node * left; //left is lower
    node * right; //right is higher
    double cost; //cost to travel along a a edge
};

struct _trunk {
    int n_links;
    node * root;
};

typedef struct{
    trunk *table;
    FILE *fp;
    int max_vertex;
} _data;

trunk * treeCreateTrunk(int n_vertex);
node * treeFindNode(node *tree, int name);
void treeCleanMem(trunk * table, int n_vertex);
void treeCut(node * tree);
void treeInitVertex(trunk *table);

void treeSeeEveryBranch(node * tree, int array[], int *i);

node * treeGetRoot(trunk * table, int key);
int treeGetLinks(trunk * table, int key);
int treeGetName(node * branch);

node * avlInsert(node * root, int name, double cost, _data data);

#endif /* Trees_h */
