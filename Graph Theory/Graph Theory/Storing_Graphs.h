//
//  Storing_Graphs.h
//  
//
//  Created by André Venceslau on 13/11/2020.
//

#ifndef Storing_Graphs_h
#define Storing_Graphs_h

#include <stdio.h>

typedef struct edge Edge;
typedef struct graphm GraphM;
typedef struct graphl GraphL;
typedef struct node link;

struct graphm {int V; int E; int **adj;};
struct graphl {int V; int E; link **adj;};
struct node {int v; link *next; int value;};
struct edge {int v; int w;};

Edge *EDGE(int, int);
void reverseList(node * head);

GraphM *GRAPHinit(int);
void GRAPHinsertE(GraphM *, Edge *);
void GRAPHremoveE(GraphM *, Edge *);
int GRAPHedges(Edge **, GraphM *);
GraphM *GRAPHcopy(GraphM *);
void GRAPHdestroy(GraphM *);

link *listFindEdge(link *list, int id);

#endif /* Storing_Graphs_h */
