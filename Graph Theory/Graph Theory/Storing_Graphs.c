//
//  Storing_Graphs.c
//  
//
//  Created by André Venceslau on 13/11/2020.
//

#include "Storing_Graphs.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void removeEdgeAdjMatrix(GraphM *G, Edge *e) {
    int v = e->v, w = e->w;
    if (G->adj[v][w] == 1) G->E--; G->adj[v][w] = 0; G->adj[w][v] = 0;
}

int numberEdgesMatrix(Edge *a[], GraphM *G) {
    int v, w, E = 0;
    for (v = 0; v < G->V; v++)
    for (w = v+1; w < G->V; w++)
    if (G->adj[v][w] == 1)
        a[E++] = EDGE(v, w);
    return E;
    
}

int **matrixInit(int number_vertex, int init_value){
    int **matrix = (int **) malloc(sizeof(int)*number_vertex*number_vertex);
    for (int i = 0 ; i < number_vertex ; i++) {
        for (int j = i+1; j < number_vertex; i++) {
            matrix[i][j] = 0;
        }
    }
    return matrix;
}

GraphM *adjMatrixInit(int V) {
    GraphM *G = (GraphM*) malloc(sizeof(struct graphm)); G->V = V; G->E = 0;
    G->adj = MATRIXinit(V, 0);
    return G;
}

void insertEdgeAdjMatrix(GraphM *G, Edge *e) {
    int v = e->v, w = e->w;
    if (G->adj[v][w] == 0) G->E++;
    G->adj[v][w] = 1;
    G->adj[w][v] = 1;
}

link *newLink(int v, link *next)
{
    link *x = (link *) malloc(sizeof(struct node)); x->v = v;
    x->next = next;
    return x;
}

GraphL *adjListInit(int V) {
    int v;
    GraphL *G = (GraphL*) malloc(sizeof(struct graphl)); G->V = V;
    G->E = 0;
    G->adj = (link **) malloc(V * sizeof(link*));
    for (v = 0; v < V; v++){
        G->adj[v] = NULL;
    }
    return G;
    
}

void insertEdgeAdjList(GraphL *G, Edge *e) {
    int v = e->v, w = e->w;
    G->adj[v] = NEW(w, G->adj[v]);
    G->adj[w] = NEW(v, G->adj[w]);
    G->E++;
}

link *listFindEdge(link *list, int id){
    link * aux = list;
    do {
        
    } while ((aux->next->v != id) || (aux->next->next != NULL));
    if (aux == NULL) {
        return NULL;
    } else {
        return aux;
    }
}

void removeEdgeAdjList (GraphL *G, Edge *e){
    link *tmp = NULL;
    link * aux = NULL;
    int v = e->v , w = e->w;
    
    if ((aux = ListFindEdge(G->adj[v], w)) != NULL) {
        tmp = aux->next->next;
        free(aux->next);
        aux->next = tmp;
        aux = ListFindEdge(G->adj[w], v);
        tmp = aux->next->next;
        free(aux->next);
        aux->next = tmp;
        G->E--;
    }
}

void reverseList(node * head) {
    node *previous;
    node *current;

    if(head != NULL) {
        previous = head;
        current = head->next;
        head = head->next;
        previous->next = NULL; // Make first node as last node
        
        while(head != NULL) {
            head = head->next;
            current->next = previous;

            previous = current;
            current = head;
        }
        head = previous; // Make last node as head
    }
}

void reverseLinkedList(node * head, node *previous) {
    if(head->next != NULL)
        reverseLinkedList(head->next, head);

    head->next = previous;
}