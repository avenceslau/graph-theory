//
//  Stacks_Queues_PQueues.c
//  Graph Theory
//
//  Created by André Venceslau on 01/03/2021.
//

#include "Stacks_Queues_PQueues.h"

/*
    This function depends on how you compare your items and should be used accordingly
 */

bool compare(void * item1, void * item2){
    /*
     here you should cast item1 and item2 to your type of variable
     compare accordingly
     */
    return true;
}

/*
    Stacks
 */

Stack * stackInit(int size){
    Stack * s = (Stack *) malloc(sizeof(Stack));
    if (s == NULL) { return NULL; }
    s->n_elements = 0;
    s->size = size;
    s->data = (void **) malloc(sizeof(8*size));
    if (s->data == NULL) {
        free(s);
        return NULL;
    }
    return s;
}

void stackPush(Stack * stack, void * item) {
    stack->data[stack->n_elements] = item;
    stack->n_elements++;
}

void * stackTop(Stack *stack) {
    return stack->data[stack->n_elements-1];
}

void stackPop(Stack * stack) {
    free(stack->data[stack->n_elements]);
    stack->n_elements--;
}

bool stackIsEmpty(Stack * stack){
    if (stack->n_elements == 0) {
        return true;
    } else {
        return false;
    }
}

void stackErase(Stack * stack){
    free(stack->data);
    free(stack);
}

void stackPrint(Stack * stack, void printItem(const void*)){
    for (int i = stack->n_elements-1; i >= 0 ; i--) {
        printItem(stack->data[i]);
    }
}

/*
    Queues
*/

Queue * queuePut(Queue * q, void * item){
    if (q == NULL) {
        q = (Queue *) malloc(sizeof(Queue));
        q->top = (qNode *) malloc(sizeof(qNode));
        q->top->item = item;
        q->top->next = NULL;
        q->bottom = q->top;
    } else {
        q->bottom->next = (qNode *) malloc(sizeof(qNode));
        q->bottom = q->bottom->next;
        q->bottom->item = item;
        q->bottom->next = NULL;
    }
    return q;
}

void * queueGet(Queue * q){
    void * tmp = q->top->item;
    qNode * tmp_node = q->top;
    q->top = q->top->next;
    free(tmp_node);
    return tmp;
}

bool queueIsEmpty(Queue * q){
    if (q->top == NULL) {
        return true;
    } else {
        return false;
    }
}

void queueErase(Queue * q){
    qNode * tmp = q->top;
    while (q->top != NULL) {
        q->top = tmp->next;
        free(tmp);
        tmp = q->top;
    }
    free(q);
}

void queuePrint(Queue * q, void printItem(const void *)){
    qNode * tmp = q->top;
    while (tmp != NULL) {
        printItem(tmp->item);
        tmp = tmp->next;
    }
}

/*
    Priority Queues
*/

void pqPrint(PQueue * q, void printItem(const void *)) {
    for (int i = 0; i < q->n_elements ; i++) {
        printItem(q->data[i]);
    }
}

void pqErase(PQueue * q){
    free(q->data);
    free(q);
}

PQueue * pqCreate(int size){
    PQueue * q = (PQueue *) malloc(sizeof(PQueue));
    if (q == NULL) { return NULL; }
    q->n_elements = 0;
    q->size = size;
    q->data = (void **) malloc(8*size);
    if (q->data == NULL) {
        free(q);
        return NULL;
    }
    return q;
}

//i = index
void FixUp(PQueue *q, int index) {
    void *t;
    while ((index > 0) && compare(q->data[(index-1) / 2], q->data[index])) {
        t = q->data[index];
        q->data[index] = q->data[(index - 1) / 2];
        q->data[(index - 1) / 2] = t;
        index = (index - 1) / 2;
    }
    return;
}

void FixDown(PQueue * q, int index) {
    int j;
    void * t;
    
    while ((2 * index + 1) < q->n_elements) {
        j = 2 * index + 1;
        if (((j + 1) < q->n_elements) && compare(q->data[j], q->data[j + 1])) {
            /* second offspring is greater */
            j++;
        }
        if (!compare(q->data[index], q->data[j])) {
            /* Elements are in correct order. */
            break;
        }
        /* the 2 elements are not correctly sorted, it is
         necessary to exchange them */
        t = q->data[index];
        q->data[index] = q->data[j];
        
        q->data[j] = t;
        index = j;
    }
    return;
}

//i = index
void SFixUp(PQueue *q, int index) {
    void * t;
    while ((index > 0) && !compare(q->data[(index - 1) / 2], q->data[index])) {
        t = q->data[index];
        
        q->data[index] = q->data[(index - 1) / 2];
        
        q->data[(index - 1) / 2] = t;
        index = (index - 1) / 2;
    }
    return;
}

void SFixDown(PQueue * q, int k) {
    int j;
    void * t;
    
    
    while ((2 * k + 1) < q->n_elements) {
        j = 2 * k + 1;
        if (((j + 1) < q->n_elements) && !compare(q->data[j], q->data[j + 1])) {
            /* second offspring is greater */
            j++;
        }
        if (compare(q->data[k], q->data[j])) {
            /* Elements are in correct order. */
            break;
        }
        /* the 2 elements are not correctly sorted, it is
         necessary to exchange them */
        t = q->data[k];
        q->data[k] = q->data[j];
        
        q->data[j] = t;
        k = j;
    }
    return;
}

void HeapSort(PQueue * q) {
    void * aux;
    int num_elements = q->n_elements;
    for (int i = q->n_elements-1 ; i > 0 ; i--) {
        aux = q->data[0];
        
        q->data[0] = q->data[i];
        
        q->data[i] = aux;
        
        q->n_elements--;
        FixDown(q, 0);
    }
    q->n_elements = num_elements;
  return;
}

void pqAdd(PQueue * q, void * item){
    q->data[q->n_elements] = item;
    q->n_elements++;
    FixUp(q, q->n_elements-1);
}

void pqModify(PQueue * q, int q_index, void * newpriority) {
    if (compare(newpriority, q->data[q_index])) {
        SFixUp(q, q_index);
    } else {
        SFixDown(q, q_index);
    }
    return;
}

void pqMinDel(PQueue * q){
    if (q->n_elements > 0) {
        q->data[0] = q->data[q->n_elements - 1];
        
        q->data[q->n_elements - 1] = 0;
        q->n_elements--;
        SFixDown(q, 0); //FIXME
    }
}

void * pqGetFirst(PQueue * q){
    void * t = q->data[0];
    return t;
}
