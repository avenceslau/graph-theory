//
//  Baders.h
//  Graph Theory
//
//  Created by André Venceslau on 01/03/2021.
//

#ifndef Baders_h
#define Baders_h

#include <stdio.h>

double getCost(int u, int v);
int GREATER(const void * A, const void * B);

#endif /* Baders_h */
